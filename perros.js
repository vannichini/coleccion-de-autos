// cree la class Perros los parametos//
function Perros(tamaño, genero, color, edad, estado) {
            this.tamaño = tamaño;
            this.genero = genero;
            this.color = color;
            this.edad = edad;
            this.estado = estado;
        }
Perros.prototype.modificar = function(adoptar) {
    this.estado = adoptar;   
}
Perros.prototype.informar = function(){
   return this.estado; 
}
// cree la funcion para poder generar nuevos perros//
function crearPerros (tamaño, genero, color, edad, estado){
    const nuevoPerro = new Perros (tamaño, genero, color, edad, estado); //le asigno un nuevo perro de la clase perros, con los mismos parametros que la funcion crearPerros//
    return nuevoPerro; //que nos devuelva el objeto creado de la clase perros//
}
// cree el array en el cual se van a contener los perros que se vayan creando//
let perrera = new Array();
//cree el do con los respectivos prompt//
do {
    const tamaño = prompt ('indicar tamaño del perro');
    const genero = prompt ('¿es hembra o macho?');
    const color = prompt ('que color tiene el perro');
    const edad = prompt ('que edad tiene el perro');
    const modifestado = prompt('modificar el estado de adopcion de un perrito:');
perrera.push (crearPerros(tamaño, genero, color, edad, modifestado)); //esto fue para que aquellos perros que se vayan generando entren en el array//
} while (window.confirm('desea cargar otro perro')); //para que una vez creado un perro, puedas seguir cargando//
console.log(perrera);

//la idea con los de abajo es que se puedan identificar los perros dentro del array, se dividan segun el estado, seguramente hay una forma mas amigable y no tan larga de hacerlo//
console.log('Perros en adopcion')
for (const estadoPerritos of perrera) {
    if (estadoPerritos.estado === 'en adopcion') {
        console.log(estadoPerritos)
    } 
}
console.log('Perros en proceso de adopcion')
for (const estadoPerritos of perrera) {
    if (estadoPerritos.estado === 'en proceso de adopcion') {
        console.log(estadoPerritos)
    } 
}
console.log('Perros adoptados')
for (const estadoPerritos of perrera) {
    if (estadoPerritos.estado === 'adoptado') {
        console.log(estadoPerritos)
    } 
}
